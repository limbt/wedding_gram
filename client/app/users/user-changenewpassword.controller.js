(function(){

    angular
        .module("weddingGramApp")
        .controller("ChangeNewPasswordCtrl", ["$state", "UserAPI", ChangeNewPasswordCtrl]);

    function ChangeNewPasswordCtrl($state, UserAPI){
        var vm = this;

        console.log(">>" + $state.params.token);
        
        vm.resetToken = $state.params.token;
        vm.NewPassword = "Password@12345";
        vm.ConfirmPassword = "Password@12345";
        vm.changePassword = changePassword;
        
        UserAPI.getLocalProfileToken(vm.resetToken)
            .then(function (result) {
                if (result.data != null) { 
                    console.log(result.data);
                } else {
                    console.log("NOT FOUND !!!! Should redirect!");
                    $state.go('SignUp');
                }
            });
            
        function changePassword(){
            console.log("Change Password ...");
            console.log(vm.NewPassword);
            console.log(vm.ConfirmPassword);

            UserAPI.changePasswordToken(
                {
                    resetToken:vm.resetToken,
                    NewPassword: vm.NewPassword,
                    ConfirmPassword: vm.ConfirmPassword
                }
            )
            .then(function(result) {
                console.log("SUCCESS!");
            })
            .catch(function(err) {
                console.log("ERROR!");
            });



            /*
            AuthFactory.changePassword(vm.user)
                .then(function () {
                    console.log("Why failing ?");
                    Flash.create('info', "Password changed.", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("ChangePassword");
                }).catch(function (error) {
                    console.error("Failed in changing password !");
                    console.error(error);
                });
            */
        }
        
    }
})();
